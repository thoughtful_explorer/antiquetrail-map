#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import geocoder
import simplekml
import re

#Set filename/path for KML file output
kmlfile = "antiquetrail.kml"
#Set KML schema name
kmlschemaname = "antiquetrail"
#Set page URL
pageURL = "http://www.antiquetrail.com/"

#Returns reusable BeautifulSoup of the site
def getsoupinit(url=pageURL):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url)
    #Return soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of all state site URLs
def getallstates():
    #Initialize the soup as a list of all links in the state-menu td
    soup = getsoupinit().find(id="state-menu").find_all("a")
    #Initialize a list to hold links
    links =[]
    #Loop through each link...
    for a in soup:
        #...and append only the link text each of them to the links list
        links.append(a["href"])
    return links

#Tests a given URL to see if the server responds. Returns True if so, raises an exception if not
def testurl(url):
	try:
		#Get Url
		get = requests.get(url)
		# if the request succeeds
		if get.status_code == 200:
			return True
		else:
			return False
	#Exception
	except requests.exceptions.RequestException as e:
        # print URL with errors
		return False

#Returns a list of store names, addresses, and lat/lng for a given site url
def getsite(url):
    #Initialize the soup
    soup = getsoupinit(url)
    #Initialize site list - sometimes we must return more than one store location in this function
    sites=[]
    #Get the relevant divs, identified by their div tag-unique styling
    divs = soup.find_all('div', attrs={'style':'font-family:Arial; font-size: 10pt; padding: 0px 0px 5px 8px;'})
    #Loop through these divs to collect name, address, and geographic information
    for div in divs:
        #Getting the store name is a simple <strong> tag within the div
        name = div.strong.get_text()
        #Getting the address involves finding a sub-div that is most easily isolated by its unique style attributes...
        addressdiv = div.find('div', attrs={'style':'font-family:Arial; font-size: 12px;'})
        #...and then stripped of its extraneous text to get the properly formatted address string
        storeaddress = re.sub('\s+',' ',addressdiv.get_text().strip()) 
        #Run the geocode on the address string
        geo = geocoder.osm(storeaddress)
        #Skip over stores with failed geocodes - it would be nice to fix the geocodes without results in a future release, but the results are satisfactory for now 
        if geo.ok == False:
            continue
        #Get the coordinates from the geocode
        lat = geo.y
        lng = geo.x
        #Append all the collected fields to the sites array
        sites.append([name,storeaddress,lat,lng])
    #Return the sites list
    return sites

#Testing for getsite()
#getsite("http://www.alabamaantiquetrail.com/")

#Returns a list of all store names, addresses, and lat/lng for all antiques trail sites
def getallstores():
    #Initialize site list
    allstores=[]
    #Loop through all state website addresses
    for state in getallstates():
        #Test whether the site's server is responding...
        if testurl(state):
            #...if so, loop through all the stores in that website... 
            for store in getsite(state):
                #...and append them to the store list...
                allstores.append(store)
        #...otherwise, print an error and move on to the next site
        else:
            print("Site "+site+" is not responding")
            continue
    return allstores

#Saves a KML file of a given list of stores
def createkml(stores):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each store
    for store in stores:
        #Get the name of the site
        storename = str(store[0])
        #Get coordinates of the site
        lat = store[2]
        lng = store[3]
        #First, create the point name and description (using the address as the description) in the kml
        point = kml.newpoint(name=storename,description=str(store[1]))
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together
createkml(getallstores())
