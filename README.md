# Antique Trail Store Map
Extracts Antique Trail store locations from the official website (and each subsequent state-based chapter website), and then places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for making https requests
    * Beautiful Soup 4.x (bs4) for scraping 
    * Geocoder for retrieving geographic coordinates for addresses from OpenStreetMap's Nominatim service 
    * Simplekml for easily building KML files
    * Regular expressions module for pattern matching
* Also of course depends on the the official [Antique Trail](http://antiquetrail.com/) website and related state-based chapter websites.
